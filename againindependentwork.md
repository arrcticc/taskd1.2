# New work
```
root@firsttemplate-1:~# docker stack deploy --with-registry-auth -c /home/arctic/task/docker-compose.yml task;
Ignoring unsupported options: restart

Creating network task_default
Creating service task_user-sim
Creating service task_catalogue-db
Creating service task_edge-router
Creating service task_carts
Creating service task_orders-db
Creating service task_user
Creating service task_front-end
Creating service task_orders
Creating service task_payment
Creating service task_carts-db
Creating service task_queue-master
Creating service task_user-db
Creating service task_catalogue
Creating service task_rabbitmq
Creating service task_shipping
```
![](2.png)
![](4.png)

```
root@firsttemplate-1:~# docker stack ls
NAME      SERVICES   ORCHESTRATOR
task      15         Swarm
```

```
root@firsttemplate-1:~# docker service ls
ID             NAME                MODE         REPLICAS   IMAGE                                PORTS
oiw8g2u84hz1   task_carts          replicated   1/1        weaveworksdemos/carts:0.4.8          
y3al8kumgjy4   task_carts-db       replicated   1/1        mongo:3.4                            
ru0xrqfn9f31   task_catalogue      replicated   1/1        weaveworksdemos/catalogue:0.3.5      
hyi34vlq5d1k   task_catalogue-db   replicated   1/1        weaveworksdemos/catalogue-db:0.3.0   
x7sydt8vv2eo   task_edge-router    replicated   1/1        weaveworksdemos/edge-router:0.1.1    *:80->80/tcp, *:8080->8080/tcp
8hc1ka9cwarh   task_front-end      replicated   1/1        weaveworksdemos/front-end:0.3.12     
wnni2v4xy7tt   task_orders         replicated   1/1        weaveworksdemos/orders:0.4.7         
j4isngxr1v2m   task_orders-db      replicated   1/1        mongo:3.4                            
qrrbg2ngtjjo   task_payment        replicated   1/1        weaveworksdemos/payment:0.4.3        
twp0vkynmwst   task_queue-master   replicated   1/1        weaveworksdemos/queue-master:0.3.1   
8ay84qhdah2d   task_rabbitmq       replicated   1/1        rabbitmq:3.6.8                       
8jmtbbxwmcls   task_shipping       replicated   1/1        weaveworksdemos/shipping:0.4.8       
```

```
root@firsttemplate-1:~# docker service update --replicas 2 task_front-end
task_front-end
overall progress: 2 out of 2 tasks 
1/2: running   [==================================================>] 
2/2: running   [==================================================>] 
verify: Service converged 

root@firsttemplate-1:~# docker service ls
ID             NAME                MODE         REPLICAS   IMAGE                                PORTS
u6phbp2taj4o   task_carts          replicated   1/1        weaveworksdemos/carts:0.4.8          
c7xwf6q5r6je   task_carts-db       replicated   1/1        mongo:3.4                            
oqz4wyz4na91   task_catalogue      replicated   1/1        weaveworksdemos/catalogue:0.3.5      
2e6gipprjhvr   task_catalogue-db   replicated   1/1        weaveworksdemos/catalogue-db:0.3.0   
r291y6bngmx1   task_edge-router    replicated   1/1        weaveworksdemos/edge-router:0.1.1    *:80->80/tcp, *:8080->8080/tcp
xx02c2r98fcd   task_front-end      replicated   2/2        weaveworksdemos/front-end:0.3.12     
```

![](5.png)